package com.gmx.cunningham.ap.IntergratedResearch;

import java.io.Serializable;

/**
 * Task for ScrumBoard
 * @author Andrew Cunningham
 * @since 29/04/2015
 */
public class Task implements Serializable{
    private String id;
    private String description;
    private TaskState state;

    /**
     * CONSTRUCTOR
     * Creates a new task with an id and description
     * @param id id for the Task
     * @param description description of the task
     */
    public Task(String id, String description){
        this.id = id;
        this.description = description;
        state = TaskState.toDo;
    }

    /**
     * Changes column of the task. Throws an exception if the move is not allowed
     * @param nState new task column
     * @throws ScrumBoardException thrown when trying to skip column or trying to move done task
     */
    public void setState(TaskState nState) throws ScrumBoardException{
        switch(state){
            case done:
                throw new ScrumBoardException(Error.stateDone);
            case toDo:
                if(nState!=TaskState.inProcess) throw new ScrumBoardException(Error.moveNotAllowed);
                state = nState;
                break;
            case inProcess:
                if(nState!=TaskState.toVerify&&nState!=TaskState.toDo) throw new ScrumBoardException(Error.moveNotAllowed);
                state = nState;
                break;
            case toVerify:
                state = nState;
                break;
        }
    }

    /**
     * Changes the description of the Task
     * @param description new description
     */
    public void setDescription(String description){
        this.description = description;
    }

    /**
     * Returns current state of the task
     * @return current state
     */
    public TaskState getState() {
        return state;
    }

    @Override
    public String toString(){
        return id+" -> "+description+" -> "+state.name();
    }
}
