package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * Created by lager on 29/04/15.
 */
public class ScrumBoardException extends Exception {
    public ScrumBoardException(Error msg){
        super(msg.getMsg());
    }
}
