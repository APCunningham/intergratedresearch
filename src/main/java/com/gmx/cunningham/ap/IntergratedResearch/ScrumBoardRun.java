package com.gmx.cunningham.ap.IntergratedResearch;

import java.io.*;
import java.util.List;

/**
 * Creates or loads a Scrum board using CLI to be used in Agile Scrum work flow
 * @author Andrew Cunningham
 * @since 29/04/2015
 */
public class ScrumBoardRun{
    //Private constants
    private static final String FILENAME = "scrum.sb";
    private static final String HELP = "Usage -- \n" +
                    "create story <id> <description>\n";
    private static final String INVCOM = "Invalid Command: ";
    private static ScrumBoard scrum;

    public static void main(String[] args){
        scrum = loadBoard(FILENAME);

        CLArgs cla = new CLArgs();

        //Tokenize the command line args
        switch(args.length) {
            default:
                StringBuilder sb = new StringBuilder();
                for(int i = 4;i<args.length;i++) sb.append(args[i]).append(" ");
                cla.setSecondDescription(sb.toString());
            case 4:
                cla.setSecondId(args[3]);
            case 3:
                cla.setFirstId(args[2]);
            case 2:
                cla.setSubCommand(args[1].toLowerCase());
            case 1:
                cla.setCommand(args[0].toLowerCase());
                break;
            case 0:
                error(HELP);
        }

        //Execute commands and handle errors
        try {
            switch (cla.getCommand()) {
                case create:
                    create(cla);
                    break;
                case list:
                    list(cla);
                    break;
                case delete:
                    delete(cla);
                    break;
                case complete:
                    complete(cla);
                    break;
                case move:
                    move(cla);
                    break;
                case update:
                    update(cla);
                    break;
                default:
                    error(INVCOM+"no command");
            }
        } catch (ScrumBoardException ex){
            error(ex.getMessage());
        }

        saveBoard(FILENAME, scrum);
    }

    // Creates new stories and tasks
    private static void create(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case story:
                checkNull(c.getFirstId(), c.getFirstDescription());
                scrum.addStory(c.getFirstId(), c.getFirstDescription());
                break;
            case task:
                checkNull(c.getFirstId(), c.getSecondId(), c.getSecondDescription());
                scrum.addTask(c.getFirstId(), c.getSecondId(), c.getSecondDescription());
                break;
            default:
                error(INVCOM+"no subcommand(create story [id] [desc], create task [id] [id] [desc])");
        }
    }

    // Lists all stories or tasks of a story
    private static void list(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case stories:
                List<Story> stories = scrum.listStories();
                print1pl(stories);
                break;
            case tasks:
                checkNull(c.getFirstId());
                List<Task> tasks = scrum.listTasks(c.getFirstId());
                print1pl(tasks);
                break;
            default:
                error(INVCOM+"no subcommand(stories, tasks)");
        }
    }

    // Deletes a story or a task
    private static void delete(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case story:
                checkNull(c.getFirstId());
                scrum.deleteStory(c.getFirstId());
                break;
            case task:
                checkNull(c.getFirstId(), c.getSecondId());
                scrum.deleteTask(c.getFirstId(), c.getSecondId());
                break;
            default:
                error(INVCOM+"no subcommand(delete story [id], delete task [id] [id])");
        }
    }

    // Completes a story
    private static void complete(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case story:
                checkNull(c.getFirstId());
                scrum.completeStory(c.getFirstId());
                break;
            default:
                error(INVCOM+"no subcommand(complete story [id])");
        }
    }

    // moves a task to another state
    private static void move(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case task:
                checkNull(c.getFirstId(), c.getSecondId());
                if(c.getColumn() == TaskState.none) error(Error.nullStr.getMsg());
                scrum.moveTask(c.getFirstId(), c.getSecondId(), c.getColumn());
                break;
            default:
                error(INVCOM+"no subcommand(move task [id] [id] [column])");
        }
    }

    // update the description of a task
    private static void update(CLArgs c) throws ScrumBoardException{
        switch(c.getSubCommand()){
            case task:
                checkNull(c.getFirstId(), c.getSecondId(), c.getSecondDescription());
                scrum.updateTask(c.getFirstId(), c.getSecondId(), c.getSecondDescription());
                break;
            default:
                error(INVCOM+"no subcommand(update task [id] [id] [desc])");
        }
    }

    // prints list 1 item per line
    private static void print1pl(List l){
        for(Object o : l){
            println(o.toString());
        }
    }

    // loads a serialized ScrumBoard
    private static ScrumBoard loadBoard(String filename){
        ScrumBoard scrum = null;
        try{
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            scrum = (ScrumBoard) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException ex){
            scrum = new ScrumBoard();
        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
            System.exit(1);
        }

        return scrum;
    }

    // save the ScrumBoard
    private static void saveBoard(String filename, ScrumBoard scrum){
        try{
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(scrum);
            oos.close();
            fos.close();
        } catch (IOException ex){
            ex.printStackTrace();
            System.exit(1);
        }
    }

    // prints strings followed by newline
    private static void println(String... str){
        print(str);
        System.out.println();
    }

    // print strings to std output
    private static void print(String... str){
        for(String s: str){
            System.out.print(s);
        }
    }

    // prints error and exits execution
    private static void error(String str){
        println(str);
        System.exit(1);
    }

    // checks if any of the string is null calls error
    private static void checkNull(String... strs){
        for(String s : strs){
            if(s==null) error(Error.nullStr.getMsg());
        }
    }
}
