package com.gmx.cunningham.ap.IntergratedResearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Story for ScrumBoard
 * @author Andrew Cunningham
 * @since 29/04/2015
 */
public class Story implements Serializable{
    private String id;
    private String description;
    private Map<String, Task> tasks;
    private boolean complete;

    /**
     * CONSTRUCTOR
     * Creates new Story to be used in ScrumBoard
     * @param id id of this story
     * @param description what the story is about
     */
    public Story(String id, String description){
        this.id = id;
        this.description = description;
        tasks = new HashMap<>();
        complete = false;
    }

    /**
     * Returns a list of tasks in this story
     * @return list of tasks
     */
    public List<Task> listTasks(){
        return new ArrayList<>(tasks.values());
    }

    /**
     * Creates and adds a new task to this story
     * @param taskId id of the new task
     * @param taskDesc description of a the new task
     * @throws ScrumBoardException when the task id already exists
     */
    public void addTask(String taskId, String taskDesc)throws ScrumBoardException{
        checkIdDNE(taskId);
        tasks.put(taskId, new Task(taskId, taskDesc));
    }

    /**
     * Completes the story.
     * @throws ScrumBoardException when a task in story is not complete
     */
    public void complete()throws ScrumBoardException{
        for(Task t : tasks.values()){
            if(t.getState()!=TaskState.done) throw new ScrumBoardException(Error.tasksNotComplete);
        }
        complete = true;
    }

    /**
     * Deletes a task from the story
     * @param taskId id of the task being deleted
     * @throws ScrumBoardException when task id does not exist
     */
    public void deleteTask(String taskId)throws ScrumBoardException{
        checkIdExist(taskId);
        tasks.remove(taskId);
    }

    /**
     * Moves a task from one column to another
     * @param taskId id the task to move
     * @param nState new column of the task
     * @throws ScrumBoardException when the task id does not exist
     */
    public void moveTask(String taskId, TaskState nState)throws ScrumBoardException{
        checkIdExist(taskId);
        tasks.get(taskId).setState(nState);
    }

    /**
     * Updates a tasks description
     * @param taskId the tasks id
     * @param nTaskDesc the new description of the task
     * @throws ScrumBoardException when task id does not exist
     */
    public void updateTask(String taskId, String nTaskDesc)throws ScrumBoardException{
        checkIdExist(taskId);
        tasks.get(taskId).setDescription(nTaskDesc);
    }

    // checks if the id exist and throws error when it does not
    private void checkIdExist(String id)throws ScrumBoardException{
        if(!tasks.containsKey(id)) throw new ScrumBoardException(Error.taskIdDNE);
    }

    // checks if the id is not already in use and throws error if it is in use
    private void checkIdDNE(String id)throws ScrumBoardException{
        if(tasks.containsKey(id)) throw new ScrumBoardException(Error.taskIdExist);
    }

    @Override
    public String toString(){
        return id+" -> "+description+(complete?" -> complete":"");
    }
}
