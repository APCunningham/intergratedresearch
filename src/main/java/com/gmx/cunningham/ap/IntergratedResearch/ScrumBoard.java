package com.gmx.cunningham.ap.IntergratedResearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds storys and tasks. The tasks are a to do list for a project.
 * To be used in an Agile work flow.
 * @author Andrew Cunningham
 * @since 29/04/2015
 */
public class ScrumBoard implements Serializable {
    private Map<String, Story> stories;

    /**
     * CONTRUCTOR
     * creates new empty Scrum board
     */
    public ScrumBoard(){
        stories = new HashMap<>();
    }

    /**
     * Creates and adds a new story to this board
     * @param id id of the new story
     * @param description description of the new story
     * @throws ScrumBoardException when story id is already in use
     */
    public void addStory(String id, String description) throws ScrumBoardException{
        checkIdDNE(id);
        stories.put(id, new Story(id, description));
    }

    /**
     * Adds new task to the story id
     * @param storyId id of the story for task to be added to
     * @param taskId id of the task
     * @param description description of the task
     * @throws ScrumBoardException when the story id does not exist
     */
    public void addTask(String storyId, String taskId, String description) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.get(storyId).addTask(taskId, description);
    }

    /**
     * Returns a list of stories
     * @return list of stories
     */
    public List<Story> listStories(){
        return new ArrayList<Story>(stories.values());
    }

    /**
     * Returns a list of tasks in a story
     * @param storyId id of the story to list tasks
     * @return lost of tasks
     * @throws ScrumBoardException when the story does not exist
     */
    public List<Task> listTasks(String storyId) throws ScrumBoardException{
        checkIdExist(storyId);
        return stories.get(storyId).listTasks();
    }

    /**
     * Completes a story if all tasks are done
     * @param storyId id of the story
     * @throws ScrumBoardException when the story id does not exist
     */
    public void completeStory(String storyId) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.get(storyId).complete();
    }

    /**
     * deletes a story and all its tasks from this scrum board
     * @param storyId id of the story
     * @throws ScrumBoardException when the story id does not exist
     */
    public void deleteStory(String storyId) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.remove(storyId);
    }

    /**
     * deletes a task from a story
     * @param storyId id of the story
     * @param taskId id of the task to delete
     * @throws ScrumBoardException when story id does not exist
     */
    public void deleteTask(String storyId, String taskId) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.get(storyId).deleteTask(taskId);
    }

    /**
     * moves a task from one column to another
     * @param storyId id of the story
     * @param taskId id of the task to move
     * @param nState column to move task to
     * @throws ScrumBoardException when the story id does not exist
     */
    public void moveTask(String storyId, String taskId, TaskState nState) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.get(storyId).moveTask(taskId, nState);
    }

    /**
     * Updates a tasks description
     * @param storyId id of the story
     * @param taskId id of the task to update
     * @param nDesc new description of the task
     * @throws ScrumBoardException when story id does not exist
     */
    public void updateTask(String storyId, String taskId, String nDesc) throws ScrumBoardException{
        checkIdExist(storyId);
        stories.get(storyId).updateTask(taskId, nDesc);
    }

    // checks if the id exist and throws error if it does not
    private void checkIdExist(String id) throws ScrumBoardException{
        if(!stories.containsKey(id)) throw new ScrumBoardException(Error.storyIdDNE);
    }

    // checks if the id is not already used and throws error if it is in use
    private void checkIdDNE(String id) throws ScrumBoardException{
        if(stories.containsKey(id)) throw new ScrumBoardException(Error.storyIdExist);
    }
}
