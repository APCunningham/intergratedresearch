package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * @author Andrew Cunningham
 * @since ${date}
 */
public enum Command {
    create, list, delete, complete, move, update, none;

    public static Command value(String act){
        try{
            return Command.valueOf(act);
        } catch (IllegalArgumentException ex){
            return Command.none;
        }
    }
}
