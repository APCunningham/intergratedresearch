package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * @author Andrew Cunningham
 * @since ${date}
 */
public enum TaskState {
    toDo, inProcess, toVerify, done, none;

    public static TaskState value(String act){
        try{
            return TaskState.valueOf(act);
        } catch (IllegalArgumentException ex){
            return TaskState.none;
        }
    }
}
