package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * Created by lager on 29/04/15.
 */
public class CLArgs {
    private Command com;
    private SubCommand scom;
    private String fid;
    private String sid;
    private String desc;

    public CLArgs() {
        com = Command.none;
        scom = SubCommand.none;
        fid = null;
        sid = null;
        desc = null;
    }

    public Command getCommand() {
        return com;
    }

    public void setCommand(String com) {
        this.com = Command.value(com);
    }

    public void setCommand(Command com) {
        this.com = com;
    }

    public SubCommand getSubCommand() {
        return scom;
    }

    public void setSubCommand(String scom) {
        this.scom = SubCommand.value(scom);
    }

    public void setSubCommand(SubCommand scom) {
        this.scom = scom;
    }

    public String getFirstId() {
        return fid;
    }

    public void setFirstId(String fid) {
        this.fid = fid;
    }

    public String getSecondId() {
        return sid;
    }

    public void setSecondId(String sid) {
        this.sid = sid;
    }

    public String getFirstDescription(){
        if(sid != null && desc != null) return sid +" "+ desc;
        return sid;
    }

    public TaskState getColumn(){
        if(desc!=null) return TaskState.value(desc.split(" ")[0]);
        return TaskState.none;
    }

    public String getSecondDescription() {
        return desc;
    }

    public void setSecondDescription(String desc) {
        this.desc = desc;
    }
}
