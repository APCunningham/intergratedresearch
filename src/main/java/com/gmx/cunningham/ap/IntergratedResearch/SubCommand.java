package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * @author Andrew Cunningham
 * @since ${date}
 */
public enum SubCommand {
    stories, story, tasks, task, none;

    public static SubCommand value(String lvl){
        try{
            return SubCommand.valueOf(lvl);
        } catch (IllegalArgumentException ex){
            return SubCommand.none;
        }
    }
}
