package com.gmx.cunningham.ap.IntergratedResearch;

/**
 * Error for the ScrumBoard
 * @author Andrew Cunningham
 * @since 29-04-2015
 */
public enum Error {
    storyIdDNE("Invalid StoryId: Id does not exist"),
    nullStr("Invalid command: command is missing"),
    storyIdExist("Invalid StoryId: Id already exists"),
    taskIdDNE("Invalid TaskId: Id does not exist"),
    taskIdExist("Invalid TaskId: Id already exists"),
    stateDone("Invalid move: Task already done"),
    moveNotAllowed("Invalid move: move not allowed"),
    tasksNotComplete("Cannot complete story: Some Task(s) is not done");

    String msg;

    Error(String msg){
        this.msg = msg;
    }

    /**
     * Gets the error message
     * @return error message
     */
    public String getMsg() {
        return msg;
    }
}
